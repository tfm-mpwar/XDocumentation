\label{ch:diseno_aplicacion}
\section{Objetivo}
El objetivo de la aplicación es el de proveer abstracciones para interactuar mediante el protocolo \gls{http} con diferentes dispositivos que pueden usar otros protocolos. Es también objetivo del diseño de esta aplicación permitir una estructura fractal (lógica o física) entre estos dispositivos, permitiendo una agrupación arbitraria de estos. El diseño tiene el objetivo de imitar la estructura para descubrir nodos descrita en \cite[45]{guinard2011web}, por este motivo deben existir relaciones entre los dispositivos lógicos o físicos.\par

Para llegar a este objetivo, hemos realizado diferentes iteraciones. En el siguiente apartado se describen algunas de estas.

\section{Iteraciones de diseño}
En este apartado se describen dos de las cuatro iteraciones que se han llevado a cabo para obtener el diseño final de la aplicación. En cada una de las cuatro iteraciones, ha existido un rápido (tiempo entre inicio y final), y algunas veces costoso (en cuanto a dedicación), tiempo de prototipado.

\subsection{Iteración 1}

Partimos con el objetivo de crear una abstracción minimalista, componible y poderosa, de las acciones que se pueden realizar sobre un dispositivo. Queríamos ir incluso más allá y crear una primitiva que nos permitiese acceder a una única acción. Estas acciones comprenden desde leer el nombre de un usuario registrado en nuestra aplicación hasta leer el valor de un contador de electricidad.\par

Por este motivo, la representación más simple era la de representar cada acción como un nodo únicamente identificable. Incluso un objeto \gls{json} se podría descomponer en nodos asociados entre ellos con la acción de recuperar su valor.\par

La manera escogida para permitir una agrupación fractal arbitraria pasaba por la representación de las relaciones entre nodos mediante un grafo. Si entre esta multitud de relaciones nos fijábamos en un nodo y seguíamos sus relaciones salientes, se podía observar una estructura de agregación, posibilitando que la acción del nodo raíz fuese la de recuperar el identificador de sus hijos directos y así sucesivamente hasta llegar a los nodos hoja. De este modo, cada nodo tendría dos tipos de relaciones salientes, una relación de acción y una relación que apuntaría a sus nodos hijos directos. Las acciones, por otra parte, también podrían agregar otras acciones, siendo esta acción raíz la acción de recolectar la información de sus acciones agregadas.\par

Durante el prototipado de esta iteración, y usando una base de datos basada en grafos de propósito general, nos dimos cuenta de que este tipo de representación tan simplista llevaba a grandes problemas de rendimiento.\\
Un ejemplo lo podemos ver en la lectura de un conjunto de datos organizados en forma arbórea (como un objeto \gls{json}). La lectura de todos estos datos supone múltiples consultas a la base de datos, hecho que reduciría mucho el rendimiento. En la \cref{fig:iter1thing} se muestra la organización de estos nodos.

\begin{figure}[h]
\centering
\includegraphics[width=1\textwidth]{graphs/GraphExample}
\caption{Composición de primitivas}
\label{fig:iter1thing}
\end{figure}

\subsection{Iteración 2}

Después de la problemática descrita en el apartado anterior, y con el objetivo de obtener un buen rendimiento de consultas y una escalabilidad en las bases de datos, observamos que se podía conseguir una estructura fractal usando una representación en un grafo de las relaciones entre los diferentes nodos y un óptimo almacenamiento de los datos usado otro tipo de base de datos que permitiese \emph{\gls{sharding}} y \emph{\gls{clustering}} de los datos.\par

Para reducir el número de nodos únicos y para aumentar la flexibilidad de cambios en el diseño, se decidió también permitir que los nuevos nodos pudiesen agrupar un conjunto de acciones. Si en un futuro se decidía restringir el número de acciones por nodo a un número determinado, sería más fácil que aumentarlo.\par

\framebox{\parbox{\dimexpr\linewidth-9\fboxsep-2\fboxrule}{\emph{En el resto del documento se usará el término \emph{\gls{thing}} para hacer referencia a las representaciones lógicas o nodos.}}}

En la \cref{fig:iter2thing} se puede observar como los \glspl{thing} y sus relaciones permiten crear una estructura fractal a la vez que los datos relacionados con cada uno de ellos se guardan en otra base de dato de más óptimo almacenamiento.\par

\begin{figure}[h]
\centering
\includegraphics[width=1\textwidth]{graphs/Iteration2}
\caption{Composición de \glspl{thing}}
\label{fig:iter2thing}
\end{figure}

\section{Diseño de las acciones}

Recordemos que el objetivo de la aplicación es el de proveer de abstracciones para interactuar con dispositivos que usen diferentes protocolos, incluso con la misma aplicación. De este objetivo se deriva que cada acción esté compuesta por un \emph{contexto} y un \emph{valor de contexto}, permitiendo a la aplicación determinar el tipo de acción a realizar y los parámetros básicos de esta acción.\par

Como ejemplo se puede pensar en una acción que recupere los datos a de una URL a través del protocolo \gls{http}. El \emph{contexto} es el uso de un cliente \gls{http} y el \emph{valor de contexto} la URL y el método HTTP a usar.\par

\section{Organización del proyecto}

Se ha decidido dividir el proyecto en dos aplicaciones. Estas dos aplicaciones corresponden al \gls{frontend} y \gls{backend} respectivamente, las funciones de dichas aplicaciones se explican en los siguientes Apartados.

\subsection{Backend}
\label{s:backendapp}
La función principal de esta aplicación es la de proporcionar una \gls{api} para crear, interactuar y borrar los \glspl{thing} y sus acciones. Su diseño permite una gestión generalizada de los \gls{thing}, es decir, permite una estructuración fractal, que un \gls{thing} contenga diferentes acciones y que la forma de los datos asociados a este sea arbitraria.\par

Proporcionará además una \gls{api} para la gestión de usuarios y una autorización de estos basada en roles y permisos. Los \emph{permisos} son entidades fijas que corresponden a los diferentes casos de uso de la aplicación (crear, borrar, actualizar o visualizar recursos tales como \glspl{thing}, permisos, roles o usuarios). Los \emph{roles} representan agrupaciones de los permisos y se asignará un rol a cada usuario.\par

\subsection{Frontend}
Esta aplicación usa la descrita en el \cref{s:backendapp}. Proporciona una interfaz visual para realizar acciones sobre la \gls{api} descrita. Esta aplicación concretiza el uso de la \gls{api}, permitiendo, por ejemplo, la visualización de los datos, siendo la aplicación la encargada de fijar la estructura de estos.\par

\section{Mockups}
Para poder validar la correcta interpretación de lo que el cliente pedía se han realizado una serie de \textit{mockups}. Dichos mockups han servido para tener una idea de la interfaz que satisfaga las necesidades de la aplicación así como para determinar qué componentes serían necesarios a la hora hacer la maquetación. Las \cref{fig:visualizacion_usuarios_tipos,fig:visualizacion_usuarios_preferencias,fig:visualizacion_estadisticas1,fig:visualizacion_estadisticas} muestran los mockups nombrados.

\begin{figure}[h]
\centering
\includegraphics[width=1\textwidth]{mockups/Users-Types_Mockup}
\caption{Visualización de Tipos de Usuario (Admin)}
\label{fig:visualizacion_usuarios_tipos}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=1\textwidth]{mockups/Preferences_Mockup}
\caption{Visualización de Preferencias de Usuario (Admin)}
\label{fig:visualizacion_usuarios_preferencias}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=1\textwidth]{mockups/Statistics_Mockup}
\caption{Visualización de Estadísticas}
\label{fig:visualizacion_estadisticas1}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=1\textwidth]{mockups/Statistics2_Mockup}
\caption{Visualización de Estadísticas}
\label{fig:visualizacion_estadisticas}
\end{figure}




