\label{ch:implementacion}
Tal y como se ha comentado en los capítulos anteriores, el proyecto se ha dividido en dos partes: una parte de \gls{backend} que se encarga de servir la \gls{api} para gestionar la estructura fractal de los \glspl{thing} así como usuarios y roles y una parte de \gls{frontend} que se nutre de esta \gls{api} y ofrecer una interfaz visual a la par que un posible uso de dicha \gls{api}. Este capítulo expone la parte de \gls{frontend} de la aplicación. En las siguientes secciones se explicará tanto la motivación del framework JavaScript utilizado como el diseño interno usado para comunicarse con la \gls{api} como consumidor de la misma.

\section{Framework JavaScript}
La elección del framework JavaScript a utilizar se ha basado en las siguientes premisas:
\begin{description}
	\item[Lenguaje de programación] Conocimiento previo del lenguaje de programación para el que se ha construido el framework (JavaScript).
	\item[Rendimiento] Rendimiento del framework a la hora de gestionar gran volumen de datos y los cambios en el \gls{dom}.
	\item[Composición] Posibilidad de crear pequeños componentes simples y acoplarlos para crear estructuras mas complejas
\end{description}

Al principio se buscaron los frameworks más usados en el sector. Entre ellos destacaron Angular, Ember, Backbone y React. Tras buscar información sobre todos ellos y comparar los resultados de las estadísticas de cada framework se decidió usar React por su simplicidad de uso y alto rendimiento.

\section{ReactJS}
ReactJS es un framework MVC de JavaScript creado por Facebook e Instagram. Los creadores lo definen como la ``V`` del patrón MVC (la vista). Una de las peculiaridades que hacen que ReactJS tenga un alto rendimiento es el echo de mantener un \gls{dom} virtual. En los siguientes apartados se comentarán más en detalle algunas de sus virtudes así como la comparación entre otros tipos de \gls{dom} virtual.

En React, cada componente tiene un estado. Gracias a este estado, React, sabe cuando repintar el componente ya que puede observar cuando cambian los datos. Se podría pensar en el uso de un ``dirty check`` pero resultaría más lento debido a la comprobación constante de los datos y la de cada dato recursivamente. Por otro lado, tener un estado que envía una señal en el momento en que se modifica permite que React simplemente escuche ese tipo de eventos y los ponga en una cola para el repintado.

\subsection{Manipulación del DOM}
Existen varias políticas a la hora de manipular el \gls{dom}, a continuación se presentan las más usadas:

\subsubsection{Virtual DOM}
Esta es la usada por ReactJS. La \cref{fig:virtual_dom} muestra un ejemplo visual. Como se puede ver en la imagen, al producirse un cambio en los datos, el \gls{dom} virtual es modificado (cuadrado rojo) y, al hacerse el algoritmo de diferenciacion, se detecta que existe una diferencia entre las dos representaciones y solo se vuelve a pintar ese componente.

El objetivo es repintar el árbol virtual solo cuando el estado cambia. Es por eso que se usa el observable (del patrón observer) en el componente para comprobar si el estado se ha modificado, es una forma eficiente de evitar repintados innecesarios lo que causaría comprobaciones de diferencias entre los árboles innecesarias. Si no hay cambios, no se hace nada.

Como puntos a favor se puede destacar que el algoritmo para diferenciar los cambios es rápido y eficiente, permite tener varias interfaces y es suficientemente ligero como para que se pueda ejecutar en dispositivos móviles. Por contra, al tener una réplica del \gls{dom} en memoria, su consumo es alto y no permite diferenciar entre elementos estáticos y dinámicos.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{VirtualDOM}
	\caption{DOM Virtual}
	\label{fig:virtual_dom}
\end{figure}

\subsubsection{Glimmer}
Este es el usado por EmberJS. Como puntos a favor destaca también un algoritmo de diferenciación eficaz y rápido, permite diferenciar entre elementos estáticos o dinámicos y la representación del \gls{dom} es ligera por lo que no consume excesiva memoria. Por contra solo permite tener una interfaz y esta enfocado solamente a ser usado con Ember. La \cref{fig:ember_glimmer} muestra de forma gráfica su funcionamiento.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Glimmer}
	\caption{Ember's Glimmer}
	\label{fig:ember_glimmer}
\end{figure}

\subsubsection{Incremental DOM}
La \cref{fig:incremental_dom} muestra gráficamente su comportamiento. Como puntos a favor destaca un uso reducido de la memoria, una \gls{api} simple y la facilidad que aporta a la hora de integrarlo con otros frameworks. Por contra no es tan eficiente como los dos vistos anteriormente y no se usa tanto en la comunidad como los anteriores.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{IncrementalDOM}
	\caption{Incremental DOM}
	\label{fig:incremental_dom}
\end{figure}

\subsection{Flux}
Flux es una arquitectura que usa Facebook internamente cuando trabaja con React. Presenta el concepto de ``Flujo Unidireccional de Datos`` (o Unidirectional Data Flow) que se muestra en la \cref{fig:flux_pattern}.
Los componentes individuales son:
\begin{description}
	\item[Actions] Son aquellos métodos que ayudan a pasar datos al Dispatcher
	\item[Dispatcher] Funciona como un sistema de publicación/suscripción. Recibe acciones y propaga los datos a todos los métodos registrados
	\item[Stores] Contienen el estado y lógica de la aplicación y tiene métodos registrados en el Dispatcher
	\item[Controller Views] Componentes de React que recogen el estado de las Stores y lo pasan a sus hijos mediante las propiedades del componente
\end{description}

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Flux}
	\caption{Patrón Flux}
	\label{fig:flux_pattern}
\end{figure}

\subsection{Redux}
Redux se puede considerar como una implementación de Flux. A grandes rasgos Redux funciona con estados inmutables de la aplicación y permite la ``navegación temporal`` entre ellos. Debido a la complejidad que añadía en comparación a lo que aportaba al proyecto (por lo menos en esta primera etapa) se optó por no usarlo.

\subsection{JSX}
JSX es una extensión de la sintaxis XML similar a ECMAScript sin ningún tipo de semántica definida. Según sus creadores, no tienen intención de que sea implementada por los navegadores ni incorporarla las especificaciones de ECMAScript. Su objetivo es ser usado por otros preprocesadores y transformarlo a ECMAScript estándar. El \cref{lst:jsx_example} muestra un ejemplo.

\begin{listing}[H]
	\begin{minted}[linenos,frame=lines, framesep=2mm,numbersep=5pt]{text}
// Uso de JSX para expresar los componentes de la UI
var dropdown =
	<Dropdown>
		Lista desplegable
		<Menu>
			<MenuItem>Hacer algo</MenuItem>
			<MenuItem>Hacer algo divertido!</MenuItem>
			<MenuItem>Hacer otra cosa</MenuItem>
		</Menu>
	</Dropdown>;
render(dropdown);
	\end{minted}
	\caption{Ejemplo de uso de JSX}
	\label{lst:jsx_example}
\end{listing}

Como se puede ver, gracias al uso de JSX, la estructura de la UI es mucho más simple a la vista por lo que agiliza el proceso de creación y composición de componentes.

\subsection{React-router}
React-router es una librería de React que se encarga del enrutado. El \cref{lst:react_router_example} muestra un ejemplo de uso:

\begin{listing}[H]
	\begin{minted}[linenos,frame=lines, framesep=2mm,numbersep=5pt]{text}
<Router history={browserHistory}>
	<Route path="/" component={App}>
		<Route path="about" component={About}/>
		<Route path="users" component={Users}>
			<Route path="/user/:userId" component={User}/>
		</Route>
		<Route path="*" component={NoMatch}/>
	</Route>
</Router>
	\end{minted}
	\caption{Ejemplo de uso de React-router}
	\label{lst:react_router_example}
\end{listing}

\section{ECMAScript 6}
Se ha usado ECMAScript 6 como lenguaje de programación para toda esta parte del proyecto. La principal razón de usar ECMAScript 6 sobre ECMAScript 5 y añadir el coste de compilarlo haciendo más costoso el tiempo de desarrollo ha sido debido al gran número de funcionalidades y \emph{azúcares sintácticos} que aporta. Algunas de estas se comentan en las siguientes secciones:

\subsection{Arrow Functions}
El principal problema que existe a la hora de pasar funciones como argumentos es la referencia \emph{this}. Existen varias soluciones para evitar este problema. La mas usada es evitar que se oculte. Una forma de hacerlo es crear una variable con nombre \emph{self} o \emph{that} y asignar a dicha variable el valor de \emph{this} por lo que al cambar de contexto mantengamos la referencia. Otra opción muy usada es mediante la función \emph{bind()}. Las \emph{arrow functions} permiten simplificar el uso de \emph{bind(this)} simplemente usando una notación especial: \code{() => {}}. En la parte de los paréntesis introducimos los parámetros que necesitemos dentro del código y entre los corchetes el código de la función en sí. Este \emph{azúcar sintáctico} añadirá por nosotros el \emph{bind(this)} para que la referencia pueda ser usada en el bloque de la función.

\subsection{Módulos}
JavaScript no cuenta con un sistema de gestión de módulos propio. Los mas usados son CommonJS y AMD. 
La principal implementación del estándar de CommonJS Modules es el que podemos encontrar en Node.js. Se caracteriza por: 
\begin{itemize}
	\item Tener una sintaxis compacta
	\item Estar pensado para la carga síncrona
	\item Ser usado principalmente en el lado del servidor
\end{itemize}
En AMD, la principal implementación es la que podemos encontrar en RequireJS. Se caracteriza por: 
\begin{itemize}
	\item Tener una sintaxis un poco más compleja
	\item Estar pensado para la carga asíncrona
	\item Ser usado principalmente en el lado del navegador
\end{itemize}

El objetivo de los módulos de ECMAScript 6 es que tanto los usuario de un sistema como el otro se sientan cómodos. Al igual que en CommonJS, comparte su sintaxis compacta, la preferencia por usar un único export y da soporte a las dependencias cíclicas. Por otro lado tiene soporte directo a la carga asíncrona y la carga de módulos configurable tal y como lo hace AMD. Además, añade algunos detalles extra:
\begin{itemize}
	\item Su sintaxis es más compacta que en CommonJS
	\item Su estructura se puede analizar de forma estática (posibilitando la optimización)
	\item El soporte que da a las dependencias cíclicas es mejor que la de CommonJS.
\end{itemize}

\subsection{Clases}
Como pasa en cualquier lenguaje de programación orientado a objetos, la creación de clases es una operación básica. ECMAScript 6 permite usar una sintaxis muy parecida a la que se usaría en dichos lenguajes para crear clases. El \cref{lst:es6_class_example} muestra un ejemplo. Como se puede ver, a parte de la creación de clases también existe la herencia. La gracia de esta sintaxis es que por debajo en realidad es una función, no obstante, su uso solo se permite si se usa seguido de la palabra \emph{new}.

\begin{listing}[H]
	\begin{minted}[linenos,frame=lines, framesep=2mm,numbersep=5pt]{javascript}
class Point {
	constructor(x, y) {
		this.x = x;
		this.y = y;
	}
	
	toString() {
		return '(' + this.x + ', ' + this.y + ')';
	}
}

class ColorPoint extends Point {
	constructor(x, y, color) {
		super(x, y);
		this.color = color;
	}

	toString() {
		return super.toString() + ' in ' + this.color;
	}
}
	\end{minted}
	\caption{Ejemplo de clases y herencia en ECMAScript 6}
	\label{lst:es6_class_example}
\end{listing}

\section{Herramientas de desarrollo adicionales}
\subsection{Babel}
Babel es un ``compilador`` de JavaScript. Para este proyecto se ha usado para compilar el código escrito en ECMAScript 6 y JSX en código ECMAScript 5 ejecutable en el navegador.

\subsection{WebPack}
WebPack es una herramienta que facilita la unificación de recursos. Para el proyecto se ha usado para generar un solo fichero de JavaScript con todo el código unificado y reducido para una carga más rápida. La \cref{fig:webpack_flow} muestra gráficamente su funcionamiento.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{what-is-webpack}
	\caption{Webpack, encapsulador de módulos}
	\label{fig:webpack_flow}
\end{figure}

\subsection{Gulp}
Gulp es una herramienta de automatización de tareas. Para el proyecto se ha usado para refrescar la página y recompilar el código durante la fase de desarrollo. Entre otras cosas era el encargado de llamar a WebPack y refrescar el navegador.

\section{Problemas encontrados}
\subsection{ReactJS}
Debido al funcionamiento de React y a la naturaleza del proyecto, ha sido difícil crear componentes pequeños y flexibles que puedan ser reutilizados en otros componentes. Un ejemplo de eso son las filas de las tablas. La idea inicial era crear un componente que dadas unas propiedades fuera capaz de representar cualquier estructura de datos en forma de fila. El componente padre, la tabla, seria el encargado de ver la estructura del objeto a representar y decidir que propiedades pasar al hijo (la fila). Por el momento, al solo tener tres tablas distintas, se han hecho a ``medida`` por lo que hay parte de código repetida entre dichos componentes.
