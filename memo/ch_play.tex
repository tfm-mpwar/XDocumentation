\label{ch:implementacion}
\section{Introducción}
Este capítulo expone la motivación del framework web utilizado y, a grandes rasgos, el diseño interno de la aplicación que expone la \gls{api}.

\section{Framework Web}
\label{sec:frameworkweb}
La elección del framework web a utilizar se ha basado en las siguientes premisas:
\begin{description}
	\item[Lenguaje de programación] Conocimiento previo del lenguaje de programación sobre el que está construido el framework.
	\item[Framework Web] Conocimiento previo del funcionamiento del framework.
	\item[Rendimiento] Rendimiento del framework y el lenguaje de programación.
	\item[Capacidades del entorno] Los lenguajes interpretados y sobretodo, el entorno donde se ejecutan (Apache, Nginx, etc.) están limitados por su modo de ejecución para realizar algunas tareas que \gls{http} permite, tales como \glspl{websocket} o Streaming.
	\item[Fácil integración] Este proyecto deberá integrarse en un futuro con otro proyecto en actual desarrollo.
\end{description}

Se dispone de conocimiento de los siguientes frameworks web: \emph{Symfony3}, \emph{Silex}, \emph{Zend Framework 2}, \emph{Phalcon PHP}, \emph{ReactPHP}\footnote{se puede considerar una librería, el soporte para \gls{http} es mínimo.} y \emph{Play! Framework 2}. A su vez y según los frameworks mencionados, se dispone de conocimiento de \emph{PHP5}, \emph{Scala} y \emph{Java}.\\
Según \emph{techempower}\footnote{\url{https://www.techempower.com/benchmarks/\#section=data-r12&hw=peak&test=fortune&l=13ykqo}}, el framework PHP que puede obtener un máximo rendimiento es Phalcon PHP, con una puntuación máxima de 38,966 (Phalcon micro). En realidad Phalcon PHP no esta desarrollado en PHP sino en \emph{Zephir}\footnote{\url{https://zephir-lang.com}}, un lenguaje con una sintaxis similar a la de PHP que compila a C, siendo entonces una extensión de PHP en C y sirviendo como framework base para aplicaciones desarrolladas en PHP. Por el contrario, Play! Framework 2 consigue una puntuación de 50,870, superando en más de 10,000 la puntuación de Phalcon PHP.\par

Las capacidades de entorno de Phalcon PHP (servidor Apache) no permiten, de forma nativa, el uso de \glspl{websocket} o Streams HTTP; son necesarias modificaciones en la configuración del servidor y, aún así, no se consigue el rendimiento deseado. En cambio, las capacidades del entorno de Play! Framework 2 (\gls{jvm}) permiten el uso de \glspl{websocket} y HTTP Streams, entre otros.\par

Las diferencias entre el rendimiento y las capacidades que ofrece cada framework se deben, en gran parte, a diferencias en el modelo de ejecución. Estos dos modelos se diferencian en el número de procesos o hilos de ejecución usados para atender una petición.


\begin{description}
	\item[Modelo de ejecución A] Relación 1 a 1 entre petición e hilo de ejecución\footnote{Aunque no estricto, se usará el término \emph{hilo de ejecución} para referirse indistintamente a \emph{hilo de ejecución} y a \emph{proceso}.} usados para atender la petición. Cada hilo de ejecución se dedica exclusivamente a atender una petición. Este modelo supone un único núcleo para ejecutar computaciones (inicios de los servidores web), los hilos de ejecución tienen el objetivo de obtener concurrencia entre peticiones, pero no paralelismo. A este modelo también se le asocia una entrada/salida de datos síncrona, por ejemplo, una petición a una base de datos bloquea el hilo de ejecución en la que se ejecuta. Si un núcleo puede soportar 1.000 hilos, durante este tiempo de espera síncrono, verá el número reducido a 999 sin que el hilo restante esté ejecutando ninguna operación de computación.
	\item[Modelo de ejecución B] Relación 1 a N entre petición e hilo de ejecución. Las acciones necesarias a realizar para atender la petición se realizan en diferentes hilos de ejecución. Este modelo supone más de un núcleo, obteniendo concurrencia y paralelismo. A este modelo se le asocia una entrada/salida de datos asíncrona y no bloqueante, esto es, al ejecutar una operación de entrada/salida, el hilo de ejecución se libera y puede empezar a atender otra petición. Cuando la operación no bloqueante finaliza, se encola la continuación de la petición. En el momento en el que un hilo esté libre, este \emph{robará el trabajo} pendiente de realizar. A este modelo de \emph{robatorio de trabajo} se le denomina \emph{work-stealing algorithm}.
\end{description}

Las \cref{fig:threaded,,fig:evented} muestran la diferencia entre los dos modelos presentados. Se puede observar que para atender el mismo número de peticiones los diferentes modelos usan cinco y dos hilos de ejecución respectivamente.\par

\begin{figure}[h]
\centering
\includegraphics[width=1\textwidth]{Threaded}
\caption{Modelo de ejecución A - \emph{Threaded} model}
\label{fig:threaded}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=1\textwidth]{Evented}
\caption{Modelo de ejecución B - \emph{Evented} model}
\label{fig:evented}
\end{figure}

No necesariamente se asocian operaciones síncronas bloqueantes y operaciones asíncronas no  bloqueantes con los modelos de ejecución \emph{A} y \emph{B} respectivamente, aunque la combinación descrita es la recurrente.\par

La mayoría de frameworks y librerías en PHP se ejecutan dentro de los parámetros descritos en el \emph{Modelo de ejecución A}, en este caso además, cada vez que un hilo de ejecución termina de procesar la petición, se termina (muere). También se ejecutan dentro de estos parámetros la mayoría de aplicaciones desarrolladas en Java, aunque desde el 2002 Java proporciona una librería pera ejecutar operaciones de entrada/salida no bloqueantes.\par

Play! Framework 2 usa el \emph{Modelo de ejecución B} y los hilos de ejecución no se terminan en acabar de procesar la petición, permitiendo una comunicación bidireccional, no bloqueante y contínua entre cliente y servidor en caso del uso de \glspl{websocket} sin necesidad de crear otro hilo de ejecución\footnote{Play! Framework 2 se construye encima de \emph{Akka}, que presenta un modelo de concurrencia y paralelismo que permite aprovechar los núcleos de computación al máximo sin necesidad de crear múltiples hilos de ejecución, más información en \url{http://cecs.wright.edu/~pmateti/Courses/7370/Lectures/Actors+Akka+Scala/akka.html}}. No obstante, aunque este modelo es potencialmente más eficiente, implica que se deba gestionar la concurrencia explícitamente. Por ejemplo, si se usa una \gls{api} bloqueante y síncrona dentro de una aplicación de este estilo, el hilo de ejecución que ejecute una llamada a dicha \gls{api} también se bloqueará y ya no atenderá más peticiones hasta que acabe la espera síncrona. Es por este motivo que se debe indicar explícitamente la llamada a la operación bloqueante e incluso ejecutarla en otro contexto.

\section{Estructura y Diseño de software}

El código del proyecto se ha estructurado siguiendo un diseño hexagonal\footnote{\url{http://alistair.cockburn.us/Hexagonal+architecture}}, esto es, dominio, aplicación e infraestructura.\par

\subsection{Organización de espacios de nombres}
En esta sección se exponen las razones que han llevado a la estructura de espacios de nombre final. Dos iteraciones han llevado a cambiar la organización de los espacios de nombres.

\paragraph{Primera iteración} En una primera iteración, la estructura principal constaba de tres espacios de nombres o \emph{namespace}s: dominio (\emph{domain}), aplicación (\emph{application}) e infraestructura (\emph{infrastructure}). Dentro de cada namespace se ubicaban todas las construcciones de cualquier tipo que tuviese sentido ubicar en los namespaces correspondientes, es decir, en dominio se ubicaban las entidades de usuario, rol, permiso y \gls{thing}; en aplicación se ubicaban los casos de uso que modificaban estas entidades, y en infrastructura las implementaciones que permitían llevar a cabo los casos de uso.\par

\paragraph{Segunda iteración} A medida que avanzaba el proyecto, la dificultad que suponía el cambio de contexto a partir de los tres namespaces descritos anteriormente empezaba a hacerse palpable. Por un lado, el hecho de disponer de todos los tipos de entidades agrupadas en un solo namespace (dominio en este caso) supone una mezcla de conceptos a veces poco relacionados (aunque cada entidad tenga su propio namespace de tercer nivel\footnote{En este punto, el primer nivel es el namespace de proyecyo, el segundo el de dominio, el tercero el de entidad.}), por otro lado, en fases algo más avanzadas del proyecto, muchos cambios suelen corresponder a un \emph{stack}\footnote{La expresión \emph{stack de la entidad} no está reconocida ni acordada por la comunidad, solo pretende agrupar el concepto de dominio, aplicación e infraestructura relacionados con una entidad, como por ejemplo la entidad de \emph{usuario}.} de la entidad  o recurso, y separar dominio, aplicación e infraestructura en el segundo nivel dificulta la navegabilidad entre recursos. Por estos motivos, se decidió cambiar la estructura de proyecto a namespace de segundo nivel por recurso, correspondiendo entonces al namespace de tercer nivel la separación entre dominio, aplicación e infraestructura.\par

\subsection{Organización de proyectos}
En esta sección se exponen las razones que han llevado a la organización final del proyecto. Dos iteraciones han llevado a cambiar la organización del proyecto. Tanto en la primera iteración como en la segunda, podemos distinguir entre el proyecto principal, esto es, la \gls{api} y dos subproyectos. A continuación se define la composición de estos según la estructura de la arquitectura hexagonal.\par

\begin{description}
	\item[core] El primero contiene dominio, aplicación e infraestructura (respositorios)
	\item[http] El segundo contiene solo la infraestructura \gls{http}
\end{description}

\paragraph{Primera iteración} Primero se decidió que cada subproyecto debía ser un proyecto separado con un repositorio \gls{git} separado. De este modo, el proyecto \emph{http} tendría como base el proyecto \emph{core} y serían dos proyectos completamente separados.

\paragraph{Segunda iteración} Diversos motivos fueron la causa del cambio en la organización de proyectos, entre ellos destacan la mantenibilidad de dos repositorios y su sincronización entre \emph{commit}s\footnote{Instantáneas de código en un momento determinado, el conjunto de \emph{commit}s de un proyecto en un repositorio forman el histórico de cambios de ese proyecto.} de los dos proyectos, la gestión de incidencias (una incidencia de cambio generalizado afecta a los dos proyectos), la navegabilidad entre proyectos durante el proceso de desarrollo, que realentizaba este debido al tiempo necesario para cambiar de proyecto, y la gestión de las versiones de las dependencias compartidas con los dos proyectos. Se decidió entonces juntar los dos proyectos en un proyecto y dos subproyectos. Esta acción permitió versionar los dos proyectos en un único repositorio; reducir el tiempo necesario para mantener dos repositorios, simplificar la gestón de las incidencias, reducir el tiempo necesario para cambiar de proyecto (navegabilidad) y la definición común de las dependencias (y sus versiones correspondientes) de los dos subproyectos.\par

La gestión de dependencias, tanto de dependencias de terceros como de las dependencias entre proyectos se ha llevado a cabo mediante la herramienta \gls{sbt}.

\subsection{Desarrollo eficiente del software}
Durante el desarrollo del proyecto se han aplicado diversas herramientas de desarrollo eficiente del software. Entre ellas destacan 1) la aquitectura hexagonal, 2) la gestión de las dependencias entre las diferentes clases mediante un \gls{dic}, 3) la aplicación de los principios \gls{solid} y 4) la aplicación de patrones de diseño, facilitados por el uso de un lenguaje funcional.\par

Destacan los patrones de diseño \emph{template method} y \emph{chain of responsibility}. El patrón template method está realmente enmascarado por las funciones de primer orden o \emph{first-class function}s\footnote{\url{https://en.wikipedia.org/wiki/First-class_function}} o, si lo miramos desde otra perspectiva, un patrón de diseño soluciona un problema común en el desarrollo de software (en este caso programación orientada a objetos), con el uso de las funciones de primer orden se soluciona este problema, ya que permite que las funciones reciban otras funciones como parámetros, siendo la función que recibe otra función como parámetro el \emph{template}. El patrón chain of responsibility lo proporciona una clase ubicada en la librería \emph{core} de Scala, la \code{PartialFunction[-A,+B]}\footnote{\url{http://www.scala-lang.org/api/2.11.8/\#scala.PartialFunction}, el método \code{orElse} epecifica que: \textit{Composes this partial function with a fallback partial function which gets applied where this partial function is not defined.}.} que permite añadir funciones de \emph{fallback} del mismo tipo en caso de que el argumento aplicado a la primera función no pueda ser tratado por esta.\par

Cabe destacar también el uso del \emph{dependency inversion principle} que ha permitido desacoplar la infraestrucutra de la aplicación y el dominio.

\subsection{Transformación de una API bloquante}
Como se ha comentado con anterioridad en el \cref{sec:frameworkweb}, el control de la concurrencia y paralelismo debe ser explícito. Para que el framework funcione a su máximo rendimiento no deben existir llamadas síncronas bloqueantes en este o, al menos, estas se deben tratar de una forma especial. El motivo es el de crear los mínimos hilos de ejecución necesarios: los suficientes para manejar el número de núcleos de computación de la máquina a máximo rendimiento sin esperas bloqueantes. No obstante, existen diversas \glspl{api} síncronas bloqueantes. La \gls{api}\footnote{Existe una \gls{api} \gls{http} pero no se ha usado debido a que el tratamiento de los datos sería más costoso para el programador.} de Neo4j que se ha usado es una de ellas.\par

Para que la \gls{api} síncrona no interfiera en el rendimiento normal del programa, se deben encapsular las llamadas síncronas en un objeto que representa una computación que aún no ha ocurrido y, además, es necesario notificar al contexto de ejecución (el conjunto de hilos de ejecución) que esa llamada es bloqueante, de este modo, el propio contexto se encargará de crear un nuevo hilo para ejecutar la llamada i no interferir con los otros hilos que trabajan intensivamente con los núcleos de computación.

\section{Documentación de la API}
Se ha documentado cada funcionalidad de la \gls{api} después de su desarrollo mediante el framework para \glspl{api} \emph{swagger}\footnote{\url{http://swagger.io}}. Swagger dispone de diferentes herramientas para editar y visualizar las documentaciones de las \glspl{api}, de este modo se han podido ir probando las diferentes funcionalidades una vez implementadas a la vez que se ha documentado el proyecto. La \cref{fig:docapi} se muestra un ejemplo de esta documentación.\par

\begin{figure}[h]
\centering
\includegraphics[width=0.75\textwidth]{DocApi}
\caption{Documentación de la API}
\label{fig:docapi}
\end{figure}

Aún así ha surgido un problema después de la implementación de la autenticación; al parecer swagger impide que las \emph{cookie}s se almacenen en el navegador y, por consiguiente, se han tenido que usar las diferentes funcionalidades de la \gls{api} mediante un cliente \gls{http}. Con los clientes \gls{http} también se han tenido problemas, algunos de ellos cortaban la cookie. Ha sido al tercer intento de probar un cliente \gls{http} que se ha podido interactuar con la \gls{api}.\par

\section{Diagramas de clases}
Muchas clases y objetos son los que componen el proyecto de \gls{backend}, en la \cref{fig:classdiagram} se muestran algunas de estas clases y su interacción. El diagrama sigue un patrón de arquitectura hexagonal. Cabe destacar el acomplamiento entre infraestructura y entidades de dominio.\par
En la \cref{fig:actiondiagram} se puede observar también la interacción entre las distintas clases para ejecutar una acción con un contexto determinado.\par

\begin{figure}[h]
\centering
\includegraphics[width=1\textwidth]{ClassDiagram}
\caption{Diagrama de clases}
\label{fig:classdiagram}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=1\textwidth]{ActionContext}
\caption{Ejecución de una Acción}
\label{fig:actiondiagram}
\end{figure}

\section{Testing}
No se ha aplicado TDD en el proyecto y la necesidad de los tests queda aliviada algo al tratarse de un lenguaje compilado i fuertemente tipado, ya que los tipos siempre serán los correctos. No obstante, al final del proyecto sí que se ha intentado hacer tests pero debido a un problema con el framework de testing que no se ha podido solucionar no se han hecho los necesarios. El problema es que los tests dan resultados aleatorios, algunas veces fallan y otras no. Sabiendo que la aplicación funciona correctamente, no debería ser así. Se ha investigado la raíz del problema  y el posible error o \textit{bug} está seguramente en las construcciones asíncronas (\code{Future[T]}) y la manera cómo se ejecutan los tests. Una explicación más detallada junto con un \emph{MWE} se pueden encontrar en \url{http://stackoverflow.com/questions/38076264/scalatest-and-scalamock-tests-not-passing-when-futures-are-involved} y en \url{https://github.com/paulbutcher/ScalaMock/issues/143}, \textit{issues} creadas específicamente para este problema.\par

\section{Proceso ETL}
El término proceso Extract Transform Load (ETL) se usa en Bases de Datos o Minería de Datos para designar a aquel proceso encargado de:

\begin{enumerate}
	\item \textbf{E}xtraer datos de una fuente. Ya sea un \textit{endpoint} \gls{http}, una base de datos, etc.
	\item \textbf{T}ransformar los datos para la ingesta de estos por otra base de datos.
	\item Cargar (\textbf{L}oad) los datos en el punto destino.
\end{enumerate}

Para poder trabajar con datos reales y un volumen más alto de datos, se ha tenido que implementar un proceso ETL. Este proceso se encarga de leer las líneas de un fichero y usar los casos de uso de la aplicación correspondientes para cargar esta información a las bases de datos. Debido a que los ficheros a cargar tienen un peso considerable para guardar en memoria (500MB) cada fichero, se han usado técnicas de \textit{streaming} para no sobrecargar la máquina virtual.

\section{Problemas observados}

\subsection{Organización de las clases}
Debido a la poca experiencia en el desarrollo de proyectos de envergadura similar a la de este, se han tenido problemas para asignar nombres y espacios de nombres a las diferentes clases que lo componen, es por este motivo que, en el histórico del proyecto (sistema de control de versiones), se puede observar una clasificación de las clases en espacios de nombres en algunos casos errática en la que posiblemente algunas clases no estén en su situación idónea.\par

\subsection{Deudas técnicas}
La transformación que ha sufrido el código del proyecto desde sus inicios hasta las últimas fases se hace notable en el hecho de que, en apenas dos semanas, la calidad del código se eleve a un nivel superior, dando así lugar a partes de código no coherentes en calidad con el resto. Debido a la demanda de nuevas funcionalidades de la \gls{api} por parte de la aplicación de \gls{frontend} y de las restricciones de tiempo, estas partes de código de menor calidad no se han mejorado.\par


